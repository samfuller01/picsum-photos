const { Picsum, defaultConfig } = require('./dist/index')


;(async function () {
  console.log(await Picsum.random())
  // {
  //   id: '391',
  //   author: 'Sarah Holmes',
  //   width: 2980,
  //   height: 2151,
  //   url: 'https://unsplash.com/photos/TlLgCqWEzUQ',
  //   download_url: 'https://picsum.photos/id/391/2980/2151.jpg'
  // }

  console.log(await Picsum.url({ height: 400, width: 300, cache: false }))
  // https://picsum.photos/400/300

  console.log(await Picsum.url({ jpg: true }))
  // https://picsum.photos/200/200.jpg?random=1625

  console.log(defaultConfig)
  // {
  //   blur: 0,
  //   cache: true,
  //   grayscale: false,
  //   height: 200,
  //   id: 0,
  //   jpg: false,
  //   width: 200
  // }
})()
