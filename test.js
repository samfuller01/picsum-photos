const { Picsum } = require('./dist/index')

;(async function () {
  const neededKeys = [
    'id',
    'author',
    'width',
    'height',
    'url',
    'download_url'
  ]
  const random = await Picsum.random()
  // Test if there are more keys than there should be
  const keys = Object.keys(random)
  keys.forEach(key => {
    if (!neededKeys.includes(key)) {
      throw new Error('Random image has key that should not be present: ' + key)
    }
  })
  // Test if all needed keys are present
  neededKeys.forEach(key => {
    if (!keys.includes(key)) {
      throw new Error('Random image is missing key: ' + key)
    }
  })

  const config = await Picsum.url({ height: 400, width: 300, cache: false })
  if (config !== 'https://picsum.photos/400/300') {
    throw new Error('Generated image URL is wrong!')
  }
  const cacheless = await Picsum.url({ jpg: true })
  if (!cacheless.includes('https://picsum.photos/200/200.jpg?random=')) {
    throw new Error('Cache busting is missing or default config is not read!')
  }
})()
  .then(() => {
    console.log('Tests passed');
  })
  .catch((error) => {
    console.log(error.message)
    console.error('Tests failed')
  })
